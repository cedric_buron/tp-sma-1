/**
* Name: werewolves
* Author: cburon
* Description: Modèle pour le premier TP du cours de l'ENSTA Systèmes MultiAgents
*/

model werewolves

/* Insert your model definition here */

global{
	int nb_personnes <- 50;
	int nb_garous <- 5;
	int nb_apothicaires <-1;
	int nb_chasseurs<-2;
	
	init{
		create personne number:nb_garous with:[est_garou::true];
		create personne number:nb_personnes with:[est_garou::false];
		create apothicaire number:nb_apothicaires;
		create chasseur number:nb_chasseurs;
	}
}

species personne skills:[moving]{
	bool est_garou;
	bool est_transforme;
	int attack_range<-4;
	
	reflex moving {
		do wander;
	}
	
	reflex attack when: !empty(personne at_distance attack_range) and est_garou and est_transforme{
		ask personne at_distance attack_range{
			self.est_garou<-true;
		}
	}
	
	reflex se_transformer when: est_garou and !est_transforme and rnd(1000)>900{
		est_transforme<-true;
	}
	
	reflex se_transformer_humain when: est_garou and est_transforme and rnd(1000)>900{
		est_transforme<-false;
	}
	
	aspect base{
		draw circle((est_transforme)?3:2) color: (est_garou) ? #black:#green;
	}
}

species apothicaire skills:[moving]{
	int healing_range<-3;

	reflex moving {
		do wander;
	}

	reflex soigner when: !empty(personne at_distance healing_range){
		ask personne at_distance healing_range{
			if(self.est_garou and !self.est_transforme){
				self.est_garou<-false;
			}
		}
	}

	aspect base{
		draw circle(2) color: #blue;
	}
}
species chasseur skills:[moving]{
	int attack_range<-7;
	
	reflex moving {
		do wander;
	}
	
	reflex chasser when: !empty(personne where (each.est_garou=true and each.est_transforme=true) at_distance attack_range){
		ask personne  where (each.est_garou=true and each.est_transforme=true) at_distance attack_range{
				do die;
		}
	}
	
	aspect base{
		draw circle(2) color: #red;
	}
}

experiment simple type:gui{
	parameter "Nombre de personnes" var: nb_personnes;
	parameter "Nombre de loups garous" var: nb_garous;
	parameter "Nombre d'apothicaires" var: nb_apothicaires;
	parameter "Nombre de chasseurs" var: nb_chasseurs;
	output{
		display village{
			species personne aspect:base;
			species apothicaire aspect:base;
			species chasseur aspect:base;
		}
		display metriques{
			chart "nb garous" size:{0.5,0.5} position:{0,0}{
				data "Nombre de loups-garous" value: length(personne where (each.est_garou=true));
			}
			chart "nb humains" size:{0.5,0.5} position:{0.5,0}{
				data "Nombre d'humains" value: length(personne where (each.est_garou=false))+length(chasseur)+length(apothicaire);
			}
			chart "total" size:{1,0.5}position:{0,0.5}{
				data "Nombre de loups-garous" value: length(personne)+length(chasseur)+length(apothicaire);
			}
		}
	}
}

experiment sans_chasseur type:gui{
	parameter "Nombre de personnes" var: nb_personnes;
	parameter "Nombre de loups garous" var: nb_garous;
	parameter "Nombre d'apothicaires" var: nb_apothicaires;
	parameter "Nombre de chasseurs" var: nb_chasseurs;
	init{
		loop i from:0 to: 5{
			create simulation with:[nb_apothicaires::i,nb_chasseurs::0];
		}
	}
	output{
		display village{
			species personne aspect:base;
			species apothicaire aspect:base;
			species chasseur aspect:base;
		}
		display metriques{
			chart "nb garous" size:{0.5,0.5} position:{0,0}{
				data "Nombre de loups-garous" value: length(personne where (each.est_garou=true));
			}
			chart "nb humains" size:{0.5,0.5} position:{0.5,0}{
				data "Nombre d'humains" value: length(personne where (each.est_garou=false))+length(chasseur)+length(apothicaire);
			}
			chart "total" size:{1,0.5}position:{0,0.5}{
				data "Nombre de loups-garous" value: length(personne)+length(chasseur)+length(apothicaire);
			}
		}
	}
}